
//function fun3(x, a, b, c, d) { return a * x**3 + b * x**2 - c * x + d;  }
function f(x, a, b, c) { return a * Math.sin(b * x) + c; }

function draw() {
	minX = -2 * Math.PI;
	minY = -10;
	maxX = 2 * Math.PI;
	maxY = 10
	init(minX, minY, maxX, maxY);
	let stepWidth = 0.05;
	
	setRGB(0,0,0);
	let a=1;
	let b=1;
	let c=0;
	for (let a=0; a < 10; a = a + 1) {
		setRGB(a * 25, 0, 0);
		for (let x = minX + stepWidth; x < maxX; x = x + stepWidth) {
			dot(x, f(x,a,b,c));
		}
	}	
}
