
let xScale = 1;
let yScale = 1;
let xMin = 0;
let xMax = 1;
let yMin = 0;
let yMax = 1;
let xOffset = 0;
let yOffset = 0;
let canvas = null;
let ctx = null;
let color = "rgb(0,0,0)";

function line(x0,y0,x1,y1) {
	y0 = canvas.height - (y0 + yOffset) * yScale;
	y1 = canvas.height - (y1 + yOffset) * yScale;
	x0 = (x0 + xOffset) * xScale;
	x1 = (x1 + xOffset) * xScale;
	ctx.strokeStyle = color;
	ctx.beginPath();
	ctx.moveTo(x0, y0);
	ctx.lineTo(x1, y1);
	ctx.stroke();
}

function dot(x,y) {
	y = canvas.height - (y + yOffset) * yScale;
	x = (x + xOffset) * xScale;
	ctx.strokeStyle = color;
	ctx.beginPath();
	ctx.moveTo(x-2, y-2);
	ctx.lineTo(x+2, y+2);
	ctx.moveTo(x-2, y+2);
	ctx.lineTo(x+2, y-2);
	ctx.stroke();
}

function drawGrid() {
	line(xMin, 0, xMax, 0);
	line(0, yMin,0, yMax);
	for (let i = Math.ceil(xMin); i < Math.floor(xMax); i++) {
		line(i, 0, i, -0.5);
	}	
	for (let i = Math.ceil(yMin); i < Math.floor(yMax); i++) {
		line((-0.5 * yScale) / xScale, i, 0, i);
	}	
}

function setRGB(r,g,b) {
	r = Math.min(Math.max(r,0), 255);
	g = Math.min(Math.max(g,0), 255);
	b = Math.min(Math.max(b,0), 255);
	color = "rgb(" + Math.floor(r) + "," + Math.floor(g) + "," + Math.floor(b) + ")";
}

function init(x0,y0,x1,y1) {
	canvas = document.getElementById("canvas");
	if (null==canvas || !canvas.getContext) return;
	ctx=canvas.getContext("2d");
	ctx.lineWidth = 2;
	xScale = Math.floor(canvas.width / (x1 - x0));
	yScale = Math.floor(canvas.height / (y1 - y0));
	xMin = x0;
	xMax = x1;
	yMin = y0;
	yMax = y1;
	xOffset = 0 - xMin;
	yOffset = 0 - yMin;
	drawGrid();
}
