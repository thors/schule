#!/bin/bash

DIRNAME=$(realpath $(dirname $0))
cd $DIRNAME
nohup firefox ${DIRNAME}/mathematik/draw_graph/my_function_plot.html &
nohup geany ${DIRNAME}/mathematik/draw_graph/my_function.js

